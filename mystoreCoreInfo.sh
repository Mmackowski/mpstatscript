#!/bin/bash

source ../demomanager/scripts/config_lte_l2l3_on_mcia.sh

while true; do
Iterator=0
mpstat -P ALL | tail -n4 | awk '{print($3, $4);}' |while read corestat; do
	CORE="$( echo $corestat | awk '{print $1}')"
	REAL_LOAD="$( echo $corestat | awk '{print $2}')"
#	REAL_LOAD=$( echo "$REAL_LOAD * 100" | bc )
	echo "wynik $CORE $REAL_LOAD"
	let "Iterator+=1";
	CPU_ROUNDED=$REAL_LOAD
	echo $CPU_ROUNDED;
	TEXT=$(mysql -u${MYSQL_USER} -p${MYSQL_PW} ${MYSQL_DATABASE} -h${MYSQL_SERVER_IP_ADDR} -e "UPDATE core_load_stats SET exec_load=${CPU_ROUNDED}, rel_load=${REAL_LOAD} WHERE core_idcore=${CORE}" 2> /dev/null)
done;
sleep 1;
done;
